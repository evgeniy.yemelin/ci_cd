module.exports = {
  apps : [{
    name      : 'APP',
    script    : 'bin/www'
  }],

  deploy : {
    production : {
      user : 'deployer',
      ssh_options: "StrictHostKeyChecking=no",
      host : '104.248.40.139',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:evgeniy.yemelin/ci_cd.git',
      path : '/srv',
      'post-deploy' : 'pm2 reload ecosystem.config.js --env production'
    }
  }
};